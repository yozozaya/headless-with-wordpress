import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { Router, Route, Switch } from 'react-router-dom'


import App from './containers/App'

import registerServiceWorker from './registerServiceWorker'
import peerlesspackApp from './reducers'
import history from './actions/history'

const thunkMiddleware = (_ref) => {
    var dispatch = _ref.dispatch
    var getState = _ref.getState
  
    return function (next) {
        return function (action) {
            if (typeof action === 'function') {
                return action(dispatch, getState)
            }
            else {
                return next(action)
            }
        }
    }
}

let store = applyMiddleware(thunkMiddleware)(createStore)(peerlesspackApp)
  

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Switch>
                <Route exact path="/" component={App} />
                <Route path="/es" component={App} />
                <Route path="/en" component={App} />
            </Switch>
        </Router>
    </Provider>
    , document.getElementById('root'))

// Every time the state changes, log it
// Note that subscribe() returns a function for unregistering the listener

/*
let unsubscribe = store.subscribe(() =>
  console.log("store: ", store.getState())
)*/

registerServiceWorker()






















