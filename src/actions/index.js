import fetch from 'isomorphic-fetch'
import Scroll from 'react-scroll'
import history from './history'

const BASE_API_URL = 'http://api.yourdomain.com.mx/wp-json/'
export const MAP_KEY = 'xxxxxxxxxxxxxxXXxxxXxXxXxXxXxXxX'

export const NAVIGATION_LANG = {
    ES: 0,
    EN: 1,
    properties: {
        0: {value: 0, name: "Español", route: "/es", tag: "es-MX", description: "Somos una empresa mexicana desde 2002"},
        1: {value: 1, name: "English", route: "/en", tag: "en-US", description: "We are a Mexican company since 2002" }
    }
}
export const MAIN_NAVIGATION = {
    ABOUT: 0,
    INDUSTRIES: 1,
    SOLUTIONS: 2,
    DESIGN: 3,
    CONTACT: 4,
    properties: {
        0: { value: 0,
            lang: [
                { category: 21, name: 'Nosotros', route: '/nosotros', anchor: "anchorAbout" },
                { category: 25, name: 'About', route: '/about', anchor: "anchorAbout" }
            ]
        },
        1: { value: 1,
            lang: [
                { category: 2, name: 'Industrias', route: '/industrias', anchor: "anchorIndustries" },
                { category: 24, name: 'Industries', route: '/industries', anchor: "anchorIndustries" }
            ]
        },
        2: { value: 2, 
            lang: [
                { category: 17, name: 'Soluciones', route: '/soluciones', anchor: "anchorSolutions" },
                { category: 9, name: 'Solutions', route: '/solutions', anchor: "anchorSolutions" }]
        },
        3: { value: 3,
            lang: [
                { category: 22, name: 'Diseño', route: '/diseno', anchor: "anchorDesign" },
                { category: 23, name: 'Design', route: '/design', anchor: "anchorDesign" }
            ]
        },
        4: { value: 4,
            lang: [
                { category: 27, name: 'Contacto', route: '/contacto', anchor: "anchorContact" },
                { category: 26, name: 'Contact', route: '/contact', anchor: "anchorContact" }
            ]
        },
    }
}

// Validate phone value for fields on forms
export const validatePhone = (phone) => {
    return /\+?(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)?(\W*\d?\W*\d{1,3})?\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*(\d{1,3})$/.test(phone)
}
// Validate email value for fields on forms
export const validateMail = (email) => {
    return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)
}


// Contact 

export const REQUEST_CONTACT_CONTENT = 'REQUEST_CONTACT_CONTENT'
export const RECEIVE_CONTACT_CONTENT = 'RECEIVE_CONTACT_CONTENT'
export const TOGGLE_SHOW_CONTACT_MODAL = 'TOGGLE_SHOW_CONTACT_MODAL'

export const requestContactContent = () => {
    return {
        type: REQUEST_CONTACT_CONTENT,
    }
}
export const receiveContactContent = (contactContent) => {
    return {
        type: RECEIVE_CONTACT_CONTENT,
        contactContent,
    }
}
export const toggleShowContact = (isOpen) => {
    return {
        type: TOGGLE_SHOW_CONTACT_MODAL,
        isOpen
    }
}
const fetchContactContent = () => {

    return (dispatch, getState) => {
        let theState = getState()
        const lang = theState.navigation.lang
        let contact = MAIN_NAVIGATION.properties[MAIN_NAVIGATION.CONTACT].lang[lang]

        let responseStatus = 0

        dispatch(requestContactContent())

        return fetch(BASE_API_URL + 'wp/v2/posts?categories=' + contact.category)

            .then(response => {

                responseStatus = response.status
                return response.ok ? response.json().then(json => {
                    dispatch(receiveContactContent(json))
                }) : response.text()

                    .then(error => Promise.reject(error)

                        .catch(error => {

                        })
                    )
            })
    }
}
const shouldFetchContactContent = (state) => {
    const contactContent = state.contact.contactContent
    if (!contactContent) {
        return true
    } else {
        return false
    }
}

export const fetchContactContentIfNeeded = () => {
    return (dispatch, getState) => {
        if (shouldFetchContactContent(getState())) {
            return dispatch(fetchContactContent())
        }
    }
}

// About 

export const REQUEST_ABOUT_CONTENT = 'REQUEST_ABOUT_CONTENT'
export const RECEIVE_ABOUT_CONTENT = 'RECEIVE_ABOUT_CONTENT'
export const TOGGLE_SHOW_ABOUT_MODAL = 'TOGGLE_SHOW_ABOUT_MODAL'

export const requestAboutContent = () => {
    return {
        type: REQUEST_ABOUT_CONTENT,
    }
}
export const receiveAboutContent = (aboutContent) => {
    return {
        type: RECEIVE_ABOUT_CONTENT,
        aboutContent,
    }
}
export const toggleShowAbout = (isOpen) => {
    return {
        type: TOGGLE_SHOW_ABOUT_MODAL,
        isOpen
    }
}
const fetchAboutContent = () => {

    return (dispatch, getState) => {
        let theState = getState()
        const lang = theState.navigation.lang
        let about = MAIN_NAVIGATION.properties[MAIN_NAVIGATION.ABOUT].lang[lang]

        let responseStatus = 0

        dispatch(requestAboutContent())

        return fetch(BASE_API_URL + 'wp/v2/posts?categories=' + about.category)

            .then(response => {

                responseStatus = response.status
                return response.ok ? response.json().then(json => {
                    dispatch(receiveAboutContent(json))
                }) : response.text()

                    .then(error => Promise.reject(error)

                        .catch(error => {

                        })
                    )
            })
    }
}
const shouldFetchAboutContent = (state) => {
    const aboutContent = state.about.aboutContent
    if (!aboutContent) {
        return true
    } else {
        return false
    }
}

export const fetchAboutContentIfNeeded = () => {
    return (dispatch, getState) => {
        if (shouldFetchAboutContent(getState())) {
            return dispatch(fetchAboutContent())
        }
    }
}

// Industries

export const REQUEST_INDUSTRIES_LIST = 'REQUEST_INDUSTRIES_LIST'
export const RECEIVE_INDUSTRIES_LIST = 'RECEIVE_INDUSTRIES_LIST'
export const TOGGLE_SHOW_INDUSTRIES_MODAL = 'TOGGLE_SHOW_INDUSTRIES_MODAL'
export const SET_SELECTED_INDUSTRY = 'SET_SELECTED_INDUSTRY'


export const requestIndustriesList = () => {
    return {
        type: REQUEST_INDUSTRIES_LIST,
    }
}
export const receiveIndustriesList = (list) => {
    return {
        type: RECEIVE_INDUSTRIES_LIST,
        list,
    }
}
export const toggleShowIndustry = (isOpen) => {
    return {
        type: TOGGLE_SHOW_INDUSTRIES_MODAL,
        isOpen
    }
}
export const setSelectedIndustry = (selectedIndustry) => {
    return {
        type: SET_SELECTED_INDUSTRY,
        selectedIndustry
    }
}
const fetchIndustriesList = () => {
    
    return (dispatch, getState) => {
        let theState = getState()
        const lang = theState.navigation.lang
        let ind = MAIN_NAVIGATION.properties[MAIN_NAVIGATION.INDUSTRIES].lang[lang]
        let responseStatus = 0
        
        dispatch( requestIndustriesList() )
        
        return fetch( BASE_API_URL + 'wp/v2/posts?categories=' + ind.category )
        
        .then(response => {
            
            responseStatus = response.status
            return response.ok ? response.json().then(json => {
                dispatch( receiveIndustriesList(json) )
            }) : response.text() 
            
            .then(error => Promise.reject(error)    
            
            .catch(error => {
                
            })
        )})
    }
}
const shouldFetchIndustriesList = (state) => {
    const list = state.industries.list
    if (!list) {
        return true
    } else {
        return false
    }
}

export const fetchIndustriesListIfNeeded = () => {
    return (dispatch, getState) => {
        if (shouldFetchIndustriesList(getState())) {
            return dispatch(fetchIndustriesList())
        }
    }
}

// Solutions

export const REQUEST_SOLUTIONS_LIST = 'REQUEST_SOLUTIONS_LIST'
export const RECEIVE_SOLUTIONS_LIST = 'RECEIVE_SOLUTIONS_LIST'
export const TOGGLE_SHOW_SOLUTIONS_MODAL = 'TOGGLE_SHOW_SOLUTIONS_MODAL'
export const SET_SELECTED_SOLUTION = 'SET_SELECTED_SOLUTION'


export const requestSolutionsList = () => {
    return {
        type: REQUEST_SOLUTIONS_LIST,
    }
}
export const receiveSolutionsList = (solutionsContent) => {
    return {
        type: RECEIVE_SOLUTIONS_LIST,
        solutionsContent,
    }
}
export const toggleShowSolution = (isOpen) => {
    return {
        type: TOGGLE_SHOW_SOLUTIONS_MODAL,
        isOpen
    }
}
export const setSelectedSolution = (selectedSolution) => {
    return {
        type: SET_SELECTED_SOLUTION,
        selectedSolution
    }
}
const fetchSolutionsList = () => {

    return (dispatch, getState) => {
        let theState = getState()
        const lang = theState.navigation.lang
        let sol = MAIN_NAVIGATION.properties[MAIN_NAVIGATION.SOLUTIONS].lang[lang]
        let responseStatus = 0

        dispatch(requestSolutionsList())

        return fetch(BASE_API_URL + 'wp/v2/posts?categories=' + sol.category)

            .then(response => {
                responseStatus = response.status
                return response.ok ? response.json().then(json => {
                    dispatch(receiveSolutionsList(json))
                }) : response.text()
                    .then(error => Promise.reject(error)
                        .catch(error => {
                        })
                    )
            })
    }
}
const shouldFetchSolutionsList = (state) => {
    const solutionsContent = state.solutions.solutionsContent
    if (!solutionsContent) {
        return true
    } else {
        return false
    }
}

export const fetchSolutiosContentIfNeeded = () => {
    return (dispatch, getState) => {
        if (shouldFetchSolutionsList(getState())) {
            return dispatch(fetchSolutionsList())
        }
    }
}


// Design 

export const REQUEST_DESIGN_CONTENT = 'REQUEST_DESIGN_CONTENT'
export const RECEIVE_DESIGN_CONTENT = 'RECEIVE_DESIGN_CONTENT'

export const requestDesignContent = () => {
    return {
        type: REQUEST_DESIGN_CONTENT,
    }
}
export const receiveDesignContent = (designContent) => {
    return {
        type: RECEIVE_DESIGN_CONTENT,
        designContent,
    }
}

const fetchDesignContent = () => {

    return (dispatch, getState) => {
        let theState = getState()
        const lang = theState.navigation.lang
        let design = MAIN_NAVIGATION.properties[MAIN_NAVIGATION.DESIGN].lang[lang]
        let responseStatus = 0

        dispatch(requestDesignContent())

        return fetch(BASE_API_URL + 'wp/v2/posts?categories=' + design.category)

            .then(response => {

                responseStatus = response.status
                return response.ok ? response.json().then(json => {
                    dispatch(receiveDesignContent(json))
                }) : response.text()

                    .then(error => Promise.reject(error)

                        .catch(error => {
                            
                        })
                    )
            })
    }
}
const shouldFetchDesignContent = (state) => {
    const designContent = state.design.designContent
    if (!designContent) {
        return true
    } else {
        return false
    }
}

export const fetchDesignContentIfNeeded = () => {
    return (dispatch, getState) => {
        if (shouldFetchDesignContent(getState())) {
            return dispatch(fetchDesignContent())
        }
    }
}


// SCROLL TO NAVIGATION

export const SET_NAVIGATION_ITEM = 'SET_NAVIGATION_ITEM'
export const TOGGLE_SHOW_MODAL_MENU = 'TOGGLE_SHOW_MODAL_MENU'
export const SET_ACTIVE_LANGUAGE = 'SET_ACTIVE_LANGUAGE'
export const SET_INITIAL_SCROLL = 'SET_INITIAL_SCROLL'
export const SET_NAVIGATION_LOADING = 'SET_NAVIGATION_LOADING'


export const toggleShowModalMenu = (isOpen) => {
    return {
        type: TOGGLE_SHOW_MODAL_MENU,
        isOpen,
    }
}
export const setInitialScroll = (initialScroll) => {
    return {
        type: SET_INITIAL_SCROLL,
        initialScroll
    }
}
export const setNavigationLoading = (scope, loaded) => {
    return {
        type: SET_NAVIGATION_LOADING,
        payload: {
            scope,
            loaded,
        }
    }
}
export const setNavigationItem = (navigationItem) => {
    return {
        type: SET_NAVIGATION_ITEM,
        navigationItem: navigationItem
    }
}
export const menuScrollTo = (target, lang, set = true) => {
    return (dispatch) => {
        const scroll = Scroll.animateScroll
        const targetId = target != null ? target.lang[lang].anchor : "anchorTop"
        const targetTag = document.getElementById( targetId )
        const targetPos = targetTag.offsetTop
        scroll.scrollTo(targetPos, { smooth: true, delay: 100, duration: 500 })
        if (set) {
            dispatch(setNavigationItem(target))
        }
        // ReactGA.event({
        //     category: 'Vehiculo',
        //     action: 'Seleccionar Versión',
        //     label: selectedItem.Name,
        //     value: selectedItem.ID
        // })
    }
}
export const setRoute = (target, additionalSlash, notScroll, set = true) => {
    return (dispatch, getState) => {
        const theState = getState()
        const lang = theState.navigation.lang
        let route = NAVIGATION_LANG.properties[lang].route
            + target.lang[lang].route

        if (additionalSlash) route = route + ('/' + additionalSlash)
        
        history.push(route)
        if (notScroll) return
        dispatch(menuScrollTo(target, lang, set))
        
    }
}

// export const removeAdditionalSlashFromRoute = (additionalSlash) => {
//     return (dispatch, getState) => {

//         history.pop(additionalSlash)
        
        
//     }
// }
export const setHome = () => {
    return (dispatch, getState) => {
        const theState = getState()
        const lang = theState.navigation.lang
        const route = NAVIGATION_LANG.properties[lang].route
        history.push(route)
        dispatch(menuScrollTo(null, lang))
    }
}
export const setActiveLanguage = (lang) => {
    return {
        type: SET_ACTIVE_LANGUAGE,
        lang,
    }
}


