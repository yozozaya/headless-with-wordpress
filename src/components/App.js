import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import history from '../actions/history'
import Scroll from 'react-scroll'
import Helmet from 'react-helmet'
import MainHeader from '../containers/Header'
import About from '../containers/About'
import Industries from '../containers/Industries'
import Solutions from '../containers/Solutions'
import Design from '../containers/Design'
import Contact from '../containers/Contact'
import Footer from './Footer'
import AboutModal from '../containers/About/AboutModal'
import IndustriesModal from '../containers/Industries/IndustriesModal'
import SolutionsModal from '../containers/Solutions/SolutionsModal'
import ModalMenu from '../containers/Menu/ModalMenu'
import '../style.css'
import { 
    NAVIGATION_LANG,
    MAIN_NAVIGATION
} from '../actions';

var Element = Scroll.Element
var globalPath = ''

class App extends Component {
    
    componentDidMount = () => {
        console.log("App", "componentDidMount")

        this.props.onInitialLoadStart()
        let fixedPath = this.props.location.pathname.replace(/\//g, "|/")
        if (fixedPath.substring(0, 1) === "|") {
            fixedPath = fixedPath.substring(1)
        }
        const splittedPath = fixedPath.split("|")
        console.log("splittedPath splittedPath splittedPath", splittedPath)
        let lang = NAVIGATION_LANG.ES
        if (splittedPath.length===0) {
            history.push("/es")
            this.props.onLoad(NAVIGATION_LANG.ES)
        }
        else {
            switch (splittedPath[0]) {
                case "/":
                    // Redirect to default lang
                    history.push("/es")
                    this.props.onLoad(NAVIGATION_LANG.ES)
                    lang = NAVIGATION_LANG.ES
                    break;
                case "/en":
                    this.props.onLoad(NAVIGATION_LANG.EN)
                    lang = NAVIGATION_LANG.EN
                    break;
                case "/es":
                default:
                    this.props.onLoad(NAVIGATION_LANG.ES)
                    lang = NAVIGATION_LANG.ES
                    break;
            }
        }
        if(splittedPath.length>1) {
            const menuItems = Object.entries(MAIN_NAVIGATION.properties)
            console.log('menuItems :::::: ', menuItems)
            const newActive = menuItems.filter((el) => 
                el[1].lang[lang].route === splittedPath[1])

            console.log('newActive :::: ', newActive )
            // If no navigationItem prop
            if (this.props.navigationItem == null && newActive.length > 0) {
                this.props.onInitialLoadStart(newActive[0][1])
            }
        }
        

    }
    shouldComponentUpdate = (nextProps) => {

        console.log("App", "shouldComponentUpdate")
        let fixedPath = nextProps.location.pathname.replace(/\//g, "|/")
        if (fixedPath.substring(0, 1) === "|") {
            fixedPath = fixedPath.substring(1)
        }
        const splittedPath = fixedPath.split("|")

        globalPath = splittedPath

        let lang = NAVIGATION_LANG.ES
        // Validate the specific language
        if (splittedPath.length === 0) {
            history.push("/es")
            this.props.onLoad(NAVIGATION_LANG.ES)
        }
        else {
            switch (splittedPath[0]) {
                case "/":
                    // Redirect to default lang
                    history.push("/es")
                    this.props.onLoad(NAVIGATION_LANG.ES)
                    lang = NAVIGATION_LANG.ES
                    break;
                case "/en":
                    this.props.onLoad(NAVIGATION_LANG.EN)
                    lang = NAVIGATION_LANG.EN
                    break;
                case "/es":
                default:
                    this.props.onLoad(NAVIGATION_LANG.ES)
                    lang = NAVIGATION_LANG.ES
                    break;
            }
        }
        if(splittedPath.length>1) {
            const menuItems = Object.entries(MAIN_NAVIGATION.properties)
            const newActive = menuItems.filter((el) => 
                el[1].lang[lang].route === splittedPath[1])
            // If no navigationItem prop
            if (nextProps.navigationItem == null && newActive.length > 0 ) {
                    nextProps.onSetNav(newActive[0][1])
                    return true
            }
        }
        
        return true
    }
    componentDidUpdate = () => {
        console.log("App", "componentDidUpdate")
        
        if (this.props.navigationItem != null
            && this.props.initialScroll === true
            && this.props.aboutLoaded === true
            && this.props.industriesLoaded === true
            && this.props.solutionsLoaded === true
            && this.props.designLoaded === true
            && this.props.contactLoaded === true
        ) {
                this.props.onInitialLoadEnd(this.props.navigationItem, this.props.lang)

                if (globalPath.length > 1) {

                    if (globalPath[1] === '/nosotros' || globalPath[1] === '/about'){
                        this.props.openAboutModal()
                    }
                }

                setTimeout(() => {

                    if (globalPath.length > 2) {

                        var wholeContentList = null
                        var lightBoxToOpen = null

                        switch (globalPath[1]) {
                            
                            case "/industrias":
                            case "/industries":
                                wholeContentList = this.props.industriesList
                                lightBoxToOpen = wholeContentList.filter((el) => el.slug === globalPath[2].split('/')[1])
                                this.props.openSlugModalIndustries(lightBoxToOpen[0])

                                break
                            case "/soluciones":
                            case "/solutions":
                                wholeContentList = this.props.solutionsList
                                lightBoxToOpen = wholeContentList.filter((el) => el.slug === globalPath[2].split('/')[1])

                                console.log('lightBoxToOpen[0] lightBoxToOpen[0] ::: ', lightBoxToOpen[0])
                                this.props.openSlugModalSolutions(lightBoxToOpen[0])
                                break
                            default:
                                break;
                        }
                    }

                }, 900);

            }
    }
    loadCallback = () => {

    }
    render = () => {
        
        console.log("App", this.props)
        const lang = this.props.lang
        const nav = this.props.navigationItem
        const title = "Peerlesspack" + (nav != null ? " - " + nav.lang[lang].name : "")
        return (
            <div>
                <Helmet>
                    <html lang={NAVIGATION_LANG.properties[lang].tag}></html>
                    <title>{title}</title>
                    <meta name="description" content={NAVIGATION_LANG.properties[lang].description} />
                </Helmet>
                <MainHeader />
                <main>
                    <Element name="anchorTop" 
                        id="anchorTop" 
                        className="element" ></Element>
                    <Element name={MAIN_NAVIGATION.properties[MAIN_NAVIGATION.ABOUT].lang[lang].anchor} 
                        id={MAIN_NAVIGATION.properties[MAIN_NAVIGATION.ABOUT].lang[lang].anchor} 
                        className="element" ></Element>
                    <About />
                    <Element name={MAIN_NAVIGATION.properties[MAIN_NAVIGATION.INDUSTRIES].lang[lang].anchor} 
                        id={MAIN_NAVIGATION.properties[MAIN_NAVIGATION.INDUSTRIES].lang[lang].anchor} 
                        className="element" ></Element>
                    <Industries />
                    <Element name={MAIN_NAVIGATION.properties[MAIN_NAVIGATION.SOLUTIONS].lang[lang].anchor} 
                        id={MAIN_NAVIGATION.properties[MAIN_NAVIGATION.SOLUTIONS].lang[lang].anchor} 
                        className="element" ></Element>
                    <Solutions />
                    <Element name={MAIN_NAVIGATION.properties[MAIN_NAVIGATION.DESIGN].lang[lang].anchor} 
                        id={MAIN_NAVIGATION.properties[MAIN_NAVIGATION.DESIGN].lang[lang].anchor} 
                        className="element" ></Element>
                    <Design />
                    <Element name={MAIN_NAVIGATION.properties[MAIN_NAVIGATION.CONTACT].lang[lang].anchor} 
                        id={MAIN_NAVIGATION.properties[MAIN_NAVIGATION.CONTACT].lang[lang].anchor} 
                        className="element" ></Element>
                    <Contact />
                </main>
                <Footer />
                <AboutModal />
                <IndustriesModal />
                <SolutionsModal />
                <ModalMenu />
            </div>
        )
    }
}
export default withRouter(App)


