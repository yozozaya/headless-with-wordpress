import React, { Component } from 'react'
import TileItem from '../containers/Tile/TileItem'
import Loader from './Loader'

export default class Industries extends Component {
    componentDidUpdate = () => {
        if (!this.props.isLoading) {
            this.props.onLoaded()
        }
	}
    renderLoader = () => {
        if (this.props.isLoading) {
            return (
                <section id="section-tile">
                    <Loader msg="Cargando..." />
                </section>
            )
        }
        else
            return null
    }
    render = () => {
        if (!this.props.isLoading) {
            if (this.props.list != null
                && this.props.list.length >= 5
            ) {
                const reversedList = this.props.list.reverse()
                return (
                    <section id="section-tile">
                        <div className="tile-left">
                            <TileItem item={reversedList[0]} />
                            <TileItem item={reversedList[1]} />
                        </div>
                        <div className="tile-right">
                            <TileItem item={reversedList[2]} />
                        </div>
                        <div className="tile-left ">
                            <TileItem item={reversedList[3]} itemClass="inv" />
                        </div>
                        <div className="tile-right">
                            <TileItem item={reversedList[4]} itemClass="single" />
                        </div>
                    </section>
                )
            }
            else
                return null
        }
        else {
            return this.renderLoader()
        }
    }
}