import React, { Component } from 'react'

export default class SolutionItemTab extends Component {
    handleClick = () => {
        this.props.onClick(this.props.index)
    }
    render = () => {
        return (
            <div className={this.props.className} onClick={this.handleClick}>
                {this.props.item.title.rendered}
            </div>)
    }
}