import React, { Component } from 'react'
import Modal from 'react-modal'
import ReactHtmlParser from 'react-html-parser'
import Slider from 'react-slick'
import '../../css/react-slick.css'

Modal.setAppElement("#root");

export default class SolutionsModal extends Component {
    handleClick = () => {
        this.props.onClick(this.props.selectedSolution)
    }
    render = () => {
        if (this.props.selectedSolution != null) {
            
            let arrayImages = []
            if (this.props.selectedSolution.acf.modal_slider_image_01 !== false)
                arrayImages.push(this.props.selectedSolution.acf.modal_slider_image_01)
            if (this.props.selectedSolution.acf.modal_slider_image_02 !== false)
                arrayImages.push(this.props.selectedSolution.acf.modal_slider_image_02)
            if (this.props.selectedSolution.acf.modal_slider_image_03 !== false)
                arrayImages.push(this.props.selectedSolution.acf.modal_slider_image_03)
            if (this.props.selectedSolution.acf.modal_slider_image_04 !== false)
                arrayImages.push(this.props.selectedSolution.acf.modal_slider_image_04)
            if (this.props.selectedSolution.acf.modal_slider_image_05 !== false)
                arrayImages.push(this.props.selectedSolution.acf.modal_slider_image_05)
            if (this.props.selectedSolution.acf.modal_slider_image_06 !== false)
                arrayImages.push(this.props.selectedSolution.acf.modal_slider_image_06)
            if (this.props.selectedSolution.acf.modal_slider_image_07 !== false)
                arrayImages.push(this.props.selectedSolution.acf.modal_slider_image_07)
            if (this.props.selectedSolution.acf.modal_slider_image_08 !== false)
                arrayImages.push(this.props.selectedSolution.acf.modal_slider_image_08)

            arrayImages = arrayImages.map((item, index) => {

                const style = {
                    backgroundImage: "url(" + item + ")",
                    width: "100%"
                }
                return <div key={index} style={style}></div>
            })

            var settings = {
                dots: false,
                arrows: false,
                autoplay: true,
                autoplaySpeed : 4000,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                pauseOnHover: false
            }

            return (
                <div className="ReactModalPortal">
                    <Modal
                        isOpen={this.props.isOpen}
                        className="modal full"
                        overlayClassName="modal-overlay"
                        onRequestClose={this.handleClick}
                        contentLabel="Solutions">
                        <div className="modal-inner" id="solution-inner">
                            <div className="modal-close" onClick={this.handleClick}>
                                <span></span>
                            </div>
                            <div className="industry-image">
                                <Slider {...settings} >
                                    {arrayImages}
                                </Slider>
                            </div>
                            <div className="industry-content">
                                <div className="industry-container">
                                    <div><h1>{this.props.selectedSolution.acf.longtitle}</h1></div>
                                    <div>
                                        {ReactHtmlParser(this.props.selectedSolution.content.rendered)}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal>
                </div>
            )
        }else
            return null    
    }
}
