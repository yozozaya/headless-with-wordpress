import React, { Component } from 'react'
import ItemMenu from './Menu/ItemMenu'
import {MAIN_NAVIGATION, NAVIGATION_LANG } from '../actions'


class MainHeader extends Component {

	handleClick = (targetId) => {
		this.props.onClick(targetId)
	}
	handleMobileClick = () => {
		this.props.onMenuClick(this.props.isOpen)
	}
    handleLangClick = (lang) => {
        this.props.onLangClick(lang)
	}
	handleLogoClick = () => {
		this.props.onLogoClick()
	}
	renderLang = (activeLang) => {

		let langs = []
		for (var lang in NAVIGATION_LANG.properties) {
			if (activeLang !== NAVIGATION_LANG.properties[lang].value) {
				langs.push(<ItemMenu targetId={lang} key={lang} onClick={this.handleLangClick} name={NAVIGATION_LANG.properties[lang].name} />)
			}
		}
		return langs
	}
	render() {
		const lang = this.props.lang != null ? this.props.lang : 0
		const nav = MAIN_NAVIGATION.properties
	  	return (
	  		<header>
			    <div className="main-logo" onClick={this.handleLogoClick}><span></span></div>
			    <div className="main-menu">
			        <div className="mobile-menu" onClick={this.handleMobileClick}><span className="icon-menu"></span></div>
			        <div className="menu-items">
						<ItemMenu 
							targetId={nav[MAIN_NAVIGATION.ABOUT]} 
							key={nav[MAIN_NAVIGATION.ABOUT].lang[lang].anchor} 
							onClick={this.handleClick} 
							name={nav[MAIN_NAVIGATION.ABOUT].lang[lang].name}/>
						<ItemMenu 
							targetId={nav[MAIN_NAVIGATION.INDUSTRIES]} 
							key={nav[MAIN_NAVIGATION.INDUSTRIES].lang[lang].anchor} 
							onClick={this.handleClick} 
							name={nav[MAIN_NAVIGATION.INDUSTRIES].lang[lang].name}/>
						<ItemMenu 
							targetId={nav[MAIN_NAVIGATION.SOLUTIONS]} 
							key={nav[MAIN_NAVIGATION.SOLUTIONS].lang[lang].anchor}
							onClick={this.handleClick} 
							name={nav[MAIN_NAVIGATION.SOLUTIONS].lang[lang].name}/>
						<ItemMenu 
							targetId={nav[MAIN_NAVIGATION.CONTACT]} 
							key={nav[MAIN_NAVIGATION.CONTACT].lang[lang].anchor} 
							onClick={this.handleClick} 
							name={nav[MAIN_NAVIGATION.CONTACT].lang[lang].name} />
						{this.renderLang(lang)}
					</div>
			    </div>
			</header>
	  	)
	}
}

export default MainHeader