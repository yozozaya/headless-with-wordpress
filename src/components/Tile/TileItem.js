import React, { Component } from 'react'
import ReactHtmlParser from 'react-html-parser'

class TileItem extends Component {
    handleClick = () => {
        this.props.onClick()
    }
    handleSelectIndustry = () => {
        this.props.onSelectIndustry(this.props.item)
    } 
    render() {
        const extraClass = this.props.itemClass
        let stringClases = ''
        
        switch (extraClass) {
            case 'single':
                stringClases = "tile-item single"
                break;

            case 'inv':
                stringClases = "tile-item inv"
                break;
        
            default:
                stringClases = "tile-item"
                break;
        }

        return (
            <div className={stringClases} onClick={this.handleSelectIndustry} >
                <div className="tile-content">
                    <div>
                        <div className="tile-box">
                            <div><span className={this.props.item.acf.icon}></span></div>
                            <div>
                                <h2>{this.props.item.title.rendered}</h2></div>
                            <div className="divider"><span></span></div>
                            <div className="tile-intro">
                                {ReactHtmlParser(this.props.item.acf.first_content)}
                            </div>
                            <div>
                                <div className="button">{this.props.item.acf.button_title}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tile-image"><img src={this.props.item.acf.desktop_image} alt={this.props.item.acf.longtitle} /></div>
                <div className="tile-image-mobile"><img src={this.props.item.acf.mobile_image} alt={this.props.item.acf.longtitle} /></div>
            </div>
        )
    }
}
export default TileItem