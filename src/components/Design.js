import React, { Component } from 'react'
import ReactHtmlParser from 'react-html-parser'

export default class Design extends Component {
    componentDidMount = () => {
		this.props.onLoaded()
	}
    render = () => {
        if (this.props.designContent != null) {
            const content = this.props.designContent
            return (
                <section id="section-design">
                    <div>
                        <div>
                            <h2>{content[0].acf.long_title}</h2>
                        </div>
                        <div className="design-img"><img src={content[0].acf.layout_image} alt="Peerlesspack Diseño de Empaques" /></div>
                        <div className="design-intro">
                            <div>
                                {ReactHtmlParser(content[0].content.rendered)}
                            </div>
                            <div>
                                <div className="button">Más Info</div>
                            </div>
                        </div>
                    </div>
                </section>
            )
        }
        else
            return null
    }
}


