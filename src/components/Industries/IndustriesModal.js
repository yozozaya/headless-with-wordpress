import React, { Component } from 'react'
import Modal from 'react-modal'
import ReactHtmlParser from 'react-html-parser'
import Slider from 'react-slick'
import '../../css/react-slick.css'

Modal.setAppElement("#root")

export default class IndustriesModal extends Component {
    handleClick = () => {
        this.props.onClick(this.props.selectedIndustry)
    }
    render = () => {

        if (this.props.selectedIndustry != null) {
            
            let arrayImages = []

            if (this.props.selectedIndustry.acf.modal_slider_image_01 !== false)
                arrayImages.push(this.props.selectedIndustry.acf.modal_slider_image_01)
            if (this.props.selectedIndustry.acf.modal_slider_image_02 !== false)
                arrayImages.push(this.props.selectedIndustry.acf.modal_slider_image_02)
            if (this.props.selectedIndustry.acf.modal_slider_image_03 !== false)
                arrayImages.push(this.props.selectedIndustry.acf.modal_slider_image_03)
            if (this.props.selectedIndustry.acf.modal_slider_image_04 !== false)
                arrayImages.push(this.props.selectedIndustry.acf.modal_slider_image_04)
            if (this.props.selectedIndustry.acf.modal_slider_image_05 !== false)
                arrayImages.push(this.props.selectedIndustry.acf.modal_slider_image_05)
            if (this.props.selectedIndustry.acf.modal_slider_image_06 !== false)
                arrayImages.push(this.props.selectedIndustry.acf.modal_slider_image_06)
            if (this.props.selectedIndustry.acf.modal_slider_image_07 !== false)
                arrayImages.push(this.props.selectedIndustry.acf.modal_slider_image_07)
            if (this.props.selectedIndustry.acf.modal_slider_image_08 !== false)
                arrayImages.push(this.props.selectedIndustry.acf.modal_slider_image_08)

            arrayImages = arrayImages.map((item, index) => {

                const style = {
                    backgroundImage: "url(" + item + ")",
                    width: "100%"
                }

                return <div key={index} style={style}></div>
            })

            var settings = {
                dots: false,
                arrows: false,
                autoplay: true,
                autoplaySpeed : 4000,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                pauseOnHover: false
            }

            return (
                <div className="ReactModalPortal">
                    <Modal
                        isOpen={this.props.isOpen}
                        className="modal full"
                        overlayClassName="modal-overlay"
                        onRequestClose={this.handleClick}
                        contentLabel="Industries">
                        <div className="modal-inner" id="food-inner">
                            <div className="modal-close" onClick={this.handleClick}>
                                <span></span>
                            </div>
                            <div className="industry-image">
                                <Slider {...settings} >
                                    {arrayImages}
                                </Slider>
                            </div>
                            <div className="industry-content">
                                <div className="industry-container">
                                    <div><span className={this.props.selectedIndustry.acf.icon}></span></div>
                                    <div><h1>{this.props.selectedIndustry.acf.longtitle}</h1></div>
                                    <div>
                                        {ReactHtmlParser(this.props.selectedIndustry.content.rendered)}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal>
                </div>
            )
        }else
            return null    
    }
}
