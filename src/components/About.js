import React, { Component } from 'react'
import ReactHtmlParser from 'react-html-parser'
import Loader from './Loader'

export default class About extends Component {
    componentDidMount = () => {
		this.props.onLoaded()
	}
	handleClick = () => {
		this.props.onClick()
	}

	renderLoader = () => {
		if (this.props.isLoading) {
			return (
				<section id="section-about">
					<div className="content-box">
						<Loader msg="" />
					</div>
				</section>
			)
		}
		else
			return null
	}
	render = () => {
		if (!this.props.isLoading) {

			if (this.props.aboutContent != null) {
				const contentAbout = this.props.aboutContent
				const bgStyle = { backgroundImage: 'url(' + contentAbout[0].acf.layout_image + ')', }

				return (
					<section id="section-about" style={bgStyle} onClick={this.handleClick}>
						<div className="content-box">
							<div className="about-content">
								<div><h2>{contentAbout[0].acf.long_title}</h2></div>
								<div>{ReactHtmlParser(contentAbout[0].acf.first_content)}</div>
								<div><div className="button">{contentAbout[0].acf.button_title}</div></div>
							</div>
						</div>
					</section>
				)
			}
			else
				return null
		}
		else {
			return this.renderLoader()
		}
	}
}



