import React, { Component } from 'react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps'
import { MAP_KEY } from '../actions';


const MapWithAMarker = withScriptjs(withGoogleMap(props =>
	<GoogleMap
		defaultZoom={8}
		defaultCenter={{ lat: -34.397, lng: 150.644 }}
	>
		<Marker
			position={{ lat: -34.397, lng: 150.644 }}
		/>
	</GoogleMap>
));

const mapsUrl = "https://maps.googleapis.com/maps/api/js?key=" + MAP_KEY + "&libraries=geometry"
class Footer extends Component {
	
	render() {
	  	return (
			    
			<footer>
				<div className="sitemap">
				<div><div className="footer-title">Industrias</div><div>Automotriz</div><div>Alimentaria</div><div>Electrónica</div><div>Consumo</div><div>Médica</div></div><div>
				<div className="footer-title">Soluciones</div><div>Charolas Termoformadas</div><div>Blisterpack</div><div>Clamshell</div><div>Press Pack</div></div><div>
				<div className="footer-title">Síguenos</div><div>Facebook</div><div>Twitter</div><div>Youtube</div></div>
						<div className="map">
							<MapWithAMarker
								googleMapURL={mapsUrl}

								
								loadingElement={<div style={{ height: `100%` }} />}
								containerElement={<div style={{ height: `250px` }} />}
								mapElement={<div style={{ height: `100%` }} />}
							/>

						</div>
				</div>
			</footer>
			
	  	)
	}
}

export default Footer