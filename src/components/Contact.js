import React, { Component } from 'react'
import Loader from './Loader'

export default class Contact extends Component {
    componentDidMount = () => {
		this.props.onLoaded()
	}
	handleClick = () => {
		this.props.onClick()
	}

	renderLoader = () => {
		if (this.props.isLoading) {
			return (
				<section id="section-contact">
					<div>
						<Loader msg="" />
					</div>
				</section>
			)
		}
		else
			return null
	}
	render = () => {
        //console.log("Contact",this.props)
		if (!this.props.isLoading) {
			if (this.props.contactContent != null && this.props.contactContent.length > 0) {
				const contentContact = this.props.contactContent
				return (
					<section id="section-contact">
						<div>
                            <div>
								<h2>{contentContact[0].title.rendered}</h2>
								<p>{contentContact[0].acf.call_text}</p>
		    					<h3>{contentContact[0].acf.phone_number}</h3>
		    					<p>{contentContact[0].acf.contact_text}</p>
								<div className="contact-form">
									<div>
										<div>{contentContact[0].acf.field_name}<span>*</span></div>
										<div><input type="text" placeholder={contentContact[0].acf.field_name} /></div>
									</div>
									<div>
										<div>{contentContact[0].acf.field_phone}<span>*</span></div>
										<div><input type="text" placeholder={contentContact[0].acf.field_phone} /></div>
									</div>
									<div className="full">
										<div>{contentContact[0].acf.field_email}<span>*</span></div>
										<div><input type="text" placeholder={contentContact[0].acf.field_email} /></div>
									</div>
									<div className="full">
										<div>{contentContact[0].acf.field_message}<span>*</span></div>
										<div><textarea placeholder={contentContact[0].acf.field_message}></textarea></div>
									</div>
									<div className="full right">
										<div className="button inverted">{contentContact[0].acf.send_label}</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				)
			}
			else
				return null
		}
		else {
			return this.renderLoader()
		}
	}
}



