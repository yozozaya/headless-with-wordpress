import React, { Component } from 'react'
import PropTypes from 'prop-types'

class ItemMenu extends Component {

    handleClick = () => {
        this.props.onClick(this.props.targetId)
    }

    render() {
        return (
            <div onClick={this.handleClick} >{this.props.name}</div>
            
        )
    }
}

ItemMenu.propTypes = {
    name: PropTypes.string.isRequired
}

export default ItemMenu