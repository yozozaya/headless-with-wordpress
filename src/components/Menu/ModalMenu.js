import React, { Component } from 'react'
import Modal from 'react-modal'
import ItemMenu from './ItemMenu'
import {MAIN_NAVIGATION, NAVIGATION_LANG } from '../../actions'


Modal.setAppElement("#root")

export default class ModalMenu extends Component {
    handleClick = () => {
        this.props.onClick()
    }
    handleItemClick = (targetId) => {
        this.props.onItemClick(targetId)
    }
    handleLangClick = (lang) => {
        this.props.onLangClick(lang)
	}
	renderLang = (activeLang) => {
		let langs = []
		for (var lang in NAVIGATION_LANG.properties) {
			if (activeLang !== NAVIGATION_LANG.properties[lang].value) {
				langs.push(<ItemMenu targetId={lang} key={lang} onClick={this.handleLangClick} name={NAVIGATION_LANG.properties[lang].name} />)
			}
		}
		return langs
	}
    render = () => {
        const lang = this.props.lang != null ? this.props.lang : 0
        const nav = MAIN_NAVIGATION.properties
        return (
            <div className="ReactModalPortal">
                <Modal
                    isOpen={this.props.isOpen}
                    className="modal menu"
                    overlayClassName="modal-overlay menu"
                    onRequestClose={this.handleClick}
                    contentLabel="Menu">
                    <div className="modal-inner menu" id="mobile-menu-modal">
                        <div className="menu-content">
                            <ItemMenu 
                                targetId={nav[MAIN_NAVIGATION.ABOUT]} 
                                key={nav[MAIN_NAVIGATION.ABOUT].lang[lang].anchor}
                                onClick={this.handleItemClick} 
                                name={nav[MAIN_NAVIGATION.ABOUT].lang[lang].name}/>
                            <ItemMenu 
                                targetId={nav[MAIN_NAVIGATION.INDUSTRIES]} 
                                key={nav[MAIN_NAVIGATION.INDUSTRIES].lang[lang].anchor} 
                                onClick={this.handleItemClick} 
                                name={nav[MAIN_NAVIGATION.INDUSTRIES].lang[lang].name}/>
                            <ItemMenu 
                                targetId={nav[MAIN_NAVIGATION.SOLUTIONS]} 
                                key={nav[MAIN_NAVIGATION.SOLUTIONS].lang[lang].anchor} 
                                onClick={this.handleItemClick} 
                                name={nav[MAIN_NAVIGATION.SOLUTIONS].lang[lang].name}/>
                            <ItemMenu 
                                targetId={nav[MAIN_NAVIGATION.CONTACT]} 
                                key={nav[MAIN_NAVIGATION.CONTACT].lang[lang].anchor} 
                                onClick={this.handleItemClick} 
                                name={nav[MAIN_NAVIGATION.CONTACT].lang[lang].name} />
                            {this.renderLang(lang)}
                        </div>
                    </div>
                </Modal>
            </div>
        )
    }
}
