import React, { Component } from 'react'
import ReactHtmlParser from 'react-html-parser'

import SolutionItemTab from './Solutions/SolutionItemTab'
import Loader from './Loader'
import {MAIN_NAVIGATION} from '../actions'

export default class Solutions extends Component {

    constructor(props, context) {
        super(props, context)

        this.state = {
            selectedTab: 0,
            tabsContent: this.props.solutionsContent != null
                ? this.props.solutionsContent.reverse() : null,
        }
    }
    componentDidUpdate = () => {
        if (!this.props.isLoading) {
            this.props.onLoaded()
        }
	}
    componentWillReceiveProps = (nextProps) => {
        this.setState({
            tabsContent: nextProps.solutionsContent != null
                ? nextProps.solutionsContent.reverse() : null
        })
    }

    handleSelectTab = (index) => {
        this.setState({
            selectedTab: index
        })
    }
    handleClick = () => {
        const index = this.state.selectedTab
        const selectedSolution = this.state.tabsContent[index]
        this.props.onClick(selectedSolution)
    }
    renderLoader = () => {
        const lang = this.props.lang != null ? this.props.lang : 0
        let loaderMsg = "Cargando..."
        switch (lang) {
            case 1:
                loaderMsg = "Loading..."
                break
            case 0:
            default:
                loaderMsg = "Cargando..."
        }
        if (this.props.isLoading) {
            return (
                <section id="section-solutions">
                    <Loader msg={loaderMsg} />
                </section>
            )
        }
        else
            return null
    }
    renderDetails = () => {
        const index = this.state.selectedTab
        if (this.state.tabsContent != null)
        return (
            <div className="solutions-details">
                <div className="details-text">
                    <div>
                        <h3>{this.state.tabsContent[index].acf.tab_header}</h3></div>
                    <div>
                        {ReactHtmlParser(this.state.tabsContent[index].acf.tab_content)}
                    </div>
                    <div>
                        <div className="button" onClick={this.handleClick}>Más Info</div>
                    </div>
                </div>
                <div className="details-img">
                    <img src={this.state.tabsContent[index].acf.tab_image} alt={this.state.tabsContent[index].title.rendered} />
                </div>
            </div>

        )
    }
    render = () => {
        const lang = this.props.lang != null ? this.props.lang : 0
        if (!this.props.isLoading) {
            if (this.state.tabsContent != null) {

                let tabsContent = this.state.tabsContent
                tabsContent = tabsContent.map((item, index) => {

                    return (
                        <SolutionItemTab className={this.state.selectedTab === index ? 'active' : ''}
                            key={index}
                            index={index}
                            onClick={this.handleSelectTab}
                            item={item} />
                    )
                })

                return (

                    <section id="section-solutions">
                        <div>
                            <div>
                                <h2>{MAIN_NAVIGATION.properties[MAIN_NAVIGATION.SOLUTIONS].lang[lang].name}</h2></div>
                            <div className="solutions-items">

                                {tabsContent}

                            </div>
                            {this.renderDetails()}
                        </div>
                    </section>


                )
            }
            else
                return null
        }
        else
            return this.renderLoader()
    }
}