import React, { Component } from 'react'
import Modal from 'react-modal'
import ReactHtmlParser from 'react-html-parser'
import Helmet from 'react-helmet'

Modal.setAppElement("#root")

export default class AboutModal extends Component {
    handleClick = () => {
        this.props.onClick()
    }
    renderHelmet = () => {
        if (this.props.isOpen) {
            return (
                <Helmet>
                    <title>Peerlesspack - {this.props.aboutContent[0].title.rendered}</title>
                    <meta name="description" content={this.props.aboutContent[0].acf.meta_description} />
                </Helmet>
            )
        }
        else
            return null
    }
    render = () => {

        if (this.props.aboutContent != null) {
            const style = {
                backgroundImage: "url(" + this.props.aboutContent[0].acf.layout_image + ")"
            }
            return (
                <div className="ReactModalPortal">
                    {this.renderHelmet()}
                    <Modal
                        isOpen={this.props.isOpen}
                        className="modal full"
                        overlayClassName="modal-overlay"
                        onRequestClose={this.handleClick}
                        contentLabel="About">
                        <div className="modal-inner" id="food-inner">
                            <div className="modal-close" onClick={this.handleClick}>
                                <span></span>
                            </div>
                            <div className="industry-image" style={style}>
                            </div>
                            <div className="industry-content">
                                <div className="industry-container">
                                    <div><span className="icon-peerlesspack"></span></div>
                                    <div><h1>{this.props.aboutContent[0].acf.long_title}</h1></div>
                                    <div>
                                        {ReactHtmlParser(this.props.aboutContent[0].content.rendered)}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal>
                </div>
            )
        } else
            return null
    }
}
