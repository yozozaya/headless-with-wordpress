import { connect } from 'react-redux'
import About from '../components/About'
import { 
    toggleShowAbout,
    setNavigationLoading, 
    setRoute,
    MAIN_NAVIGATION
} from '../actions'

const mapStateToProps = (state) => {
    const { aboutContent, isLoading } = state.about
    const {lang} = state.navigation
    return {
        aboutContent,
        isLoading,
        lang
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onClick: (isOpen) => {
            const nav = MAIN_NAVIGATION.properties
            dispatch(toggleShowAbout(isOpen))
            dispatch(setRoute(nav[MAIN_NAVIGATION.ABOUT], false, true))
        },
        onLoaded: () => {
            dispatch(setNavigationLoading('about', true))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(About)
