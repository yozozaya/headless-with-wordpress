import { connect } from 'react-redux'
import {
    setNavigationLoading,
} from '../actions'
import Industries from '../components/Industries'

const mapStateToProps = (state) => {
    const { list, isLoading } = state.industries
    return {
        list,
        isLoading,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onLoaded: () => {
            dispatch(setNavigationLoading('industries', true))
        }
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Industries)
