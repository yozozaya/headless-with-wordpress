import { connect } from 'react-redux'
import {
    toggleShowModalMenu,
    setRoute,
    setActiveLanguage, 
    receiveAboutContent, 
    receiveIndustriesList, 
    receiveSolutionsList, 
    receiveDesignContent, 
    fetchAboutContentIfNeeded, 
    fetchIndustriesListIfNeeded, 
    fetchSolutiosContentIfNeeded, 
    fetchDesignContentIfNeeded, 
    receiveContactContent,
    fetchContactContentIfNeeded,
    setHome
} from '../../actions'
import ModalMenu from '../../components/Menu/ModalMenu'
const mapStateToProps = (state) => {
    const { isOpen, lang } = state.navigation
    return { isOpen, lang }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onClick: () => {
            dispatch(toggleShowModalMenu(true))
        },
        onItemClick: (target) => {
            dispatch(setRoute(target))
            dispatch(toggleShowModalMenu(true))
        },
        onLangClick: (lang) => {
            // Set the new language
            dispatch(setActiveLanguage(lang))
            // Clear the old language content
            dispatch(receiveAboutContent(null))
            dispatch(receiveIndustriesList(null))
            dispatch(receiveSolutionsList(null))
            dispatch(receiveDesignContent(null))
            dispatch(receiveContactContent(null))
            // Fetch the new language content
            dispatch(fetchAboutContentIfNeeded())
            dispatch(fetchIndustriesListIfNeeded())
            dispatch(fetchSolutiosContentIfNeeded())
            dispatch(fetchDesignContentIfNeeded())
            dispatch(fetchContactContentIfNeeded())
            // Close the modal
            dispatch(toggleShowModalMenu(true))
            // Redirect to route
            dispatch(setHome())
        }
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ModalMenu)