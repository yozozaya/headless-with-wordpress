import { connect } from 'react-redux'
import {
    setNavigationLoading,
} from '../actions'
import Design from '../components/Design'

const mapStateToProps = (state) => {
    const { designContent, isLoading } = state.design
    return {
        designContent,
        isLoading,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onLoaded: () => {
            dispatch(setNavigationLoading('design', true))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Design)
