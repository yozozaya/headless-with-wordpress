import { connect } from 'react-redux'
import {
    setNavigationLoading,
} from '../actions'
import Contact from '../components/Contact'

const mapStateToProps = (state) => {
    const { contactContent, isLoading } = state.contact
    return {
        contactContent,
        isLoading,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onLoaded: () => {
            dispatch(setNavigationLoading('contact', true))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Contact)
