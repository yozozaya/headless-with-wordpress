import { connect } from 'react-redux'
import Header from '../components/Header'
import { setRoute, 
    toggleShowModalMenu, 
    setActiveLanguage, 
    receiveAboutContent, 
    receiveIndustriesList, 
    receiveSolutionsList, 
    receiveDesignContent, 
    fetchAboutContentIfNeeded, 
    fetchIndustriesListIfNeeded, 
    fetchSolutiosContentIfNeeded, 
    fetchDesignContentIfNeeded, 
    receiveContactContent,
    fetchContactContentIfNeeded,
    setHome
} from '../actions'

const mapStateToProps = (state) => {
    const {isOpen, lang} = state.navigation
    return { isOpen, lang }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onClick: (target) => {
            dispatch(setRoute(target))
        },
        onLogoClick: () => {
            dispatch(setHome())
        },
        onMenuClick: (isOpen) => {
            dispatch(toggleShowModalMenu(isOpen))
        },
        onLangClick: (lang) => {
            // Set the new language
            dispatch(setActiveLanguage(lang))
            // Clear the old language content
            dispatch(receiveAboutContent(null))
            dispatch(receiveIndustriesList(null))
            dispatch(receiveSolutionsList(null))
            dispatch(receiveDesignContent(null))
            dispatch(receiveContactContent(null))
            // Fetch the new language content
            dispatch(fetchAboutContentIfNeeded())
            dispatch(fetchIndustriesListIfNeeded())
            dispatch(fetchSolutiosContentIfNeeded())
            dispatch(fetchDesignContentIfNeeded())
            dispatch(fetchContactContentIfNeeded())
            dispatch(setHome())
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header)
