import { connect } from 'react-redux'
import { 
    setActiveLanguage,
    setHome,
    setNavigationItem,
    menuScrollTo,
    setInitialScroll,
    fetchAboutContentIfNeeded,
    fetchIndustriesListIfNeeded,
    fetchSolutiosContentIfNeeded,
    fetchDesignContentIfNeeded,
    fetchContactContentIfNeeded,

    toggleShowAbout,
    setSelectedIndustry,
    toggleShowIndustry,
    setSelectedSolution,
    toggleShowSolution

} from '../actions'
import App from '../components/App'

const mapStateToProps = (state) => {
    const { 
        lang, 
        navigationItem, 
        initialScroll,
        aboutLoaded,
        industriesLoaded,
        solutionsLoaded,
        designLoaded,
        contactLoaded
    } = state.navigation
    const industriesList = state.industries.list
    const solutionsList = state.solutions.solutionsContent
    return { 
        lang, 
        navigationItem, 
        initialScroll,
        aboutLoaded,
        industriesLoaded,
        solutionsLoaded,
        designLoaded,
        contactLoaded,
        industriesList,
        solutionsList
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onSetLang: (lang) => {
            dispatch(setActiveLanguage(lang))
            dispatch(setHome())
        },
        onLoad: (lang) => {
            dispatch(setActiveLanguage(lang))
            dispatch(fetchAboutContentIfNeeded())
            dispatch(fetchIndustriesListIfNeeded())
            dispatch(fetchSolutiosContentIfNeeded())
            dispatch(fetchDesignContentIfNeeded())
            dispatch(fetchContactContentIfNeeded())
        },
        onSetNav: (navigationItem) => {
            dispatch(setNavigationItem(navigationItem))
        },
        onInitialLoadStart: (navigationItem) => {
            dispatch(setInitialScroll(true))
            if (navigationItem != null) {
                dispatch(setNavigationItem(navigationItem))
            }
        },
        onInitialLoadEnd: (navigationItem, lang) => {
            dispatch(menuScrollTo(navigationItem, lang, false))
            dispatch(setInitialScroll(false))
        },
        openAboutModal: () =>{
            dispatch(toggleShowAbout(false))
        },
        openSlugModalIndustries: (selectedIndustry) => {
            dispatch(setSelectedIndustry(selectedIndustry))
            dispatch(toggleShowIndustry(false))
        },
        openSlugModalSolutions: (selectedSolution) => {
            
            dispatch(toggleShowSolution(false))
            dispatch(setSelectedSolution(selectedSolution))
        }
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
