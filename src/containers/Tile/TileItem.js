import { connect } from 'react-redux'
import TileItem from '../../components/Tile/TileItem'
import { toggleShowIndustry, setSelectedIndustry, setRoute, MAIN_NAVIGATION } from '../../actions';


const mapStateToProps = (state) => {
    const { isLoading } = state.industries
    return {
        isLoading,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onClick: (isOpen) => {
            dispatch(toggleShowIndustry(isOpen))
        },
        onSelectIndustry: (selectedIndustry) => {

            const nav = MAIN_NAVIGATION.properties 

            dispatch(setSelectedIndustry(selectedIndustry))
            dispatch(toggleShowIndustry(false))
            dispatch(setRoute(nav[MAIN_NAVIGATION.INDUSTRIES], selectedIndustry.slug, true ))
        }
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TileItem)
