import { connect } from 'react-redux'
import {
    toggleShowIndustry,
    setSelectedIndustry,
    setRoute,
    MAIN_NAVIGATION
} from '../../actions'
import IndustriesModal from '../../components/Industries/IndustriesModal'

const mapStateToProps = (state) => {
    const { isLoading,
        selectedIndustry,
        isOpen
         } = state.industries
    return {
        isLoading,
        isOpen,
        selectedIndustry
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClick: (selectedIndustry) => {
            const nav = MAIN_NAVIGATION.properties
            dispatch(toggleShowIndustry(true))
            dispatch(setSelectedIndustry(null))
            dispatch(setRoute( nav[MAIN_NAVIGATION.INDUSTRIES], false, true ))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IndustriesModal)