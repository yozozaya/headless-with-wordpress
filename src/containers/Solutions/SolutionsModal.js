import { connect } from 'react-redux'
import {
    toggleShowSolution,
    setSelectedSolution,
    setRoute,
    MAIN_NAVIGATION

} from '../../actions'
import SolutionsModal from '../../components/Solutions/SolutionsModal'

const mapStateToProps = (state) => {
    const { isLoading,
        selectedSolution,
        isOpen,
    } = state.solutions
    return {
        isLoading,
        isOpen,
        selectedSolution,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClick: (selectedSolution) => {
            const nav = MAIN_NAVIGATION.properties 
            dispatch(toggleShowSolution(true))
            dispatch(setSelectedSolution(null))
            //dispatch(setSelectedSolution(selectedSolution))
            dispatch(setRoute(nav[MAIN_NAVIGATION.SOLUTIONS], false, true))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SolutionsModal)