import { connect } from 'react-redux'
import Solutions from '../components/Solutions'
import { 
    setSelectedSolution, 
    toggleShowSolution, 
    setNavigationLoading,
    setRoute,
    MAIN_NAVIGATION

} from '../actions';

const mapStateToProps = (state) => {
    const { 
        isLoading,
        solutionsContent,
        selectedSolution,
    } = state.solutions
    const {lang} = state.navigation
    return {
        isLoading,
        solutionsContent,
        selectedSolution,
        lang
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClick: (selectedSolution) => {
            const nav = MAIN_NAVIGATION.properties
            dispatch(setSelectedSolution(selectedSolution))
            dispatch(toggleShowSolution(false))
            dispatch(setRoute(nav[MAIN_NAVIGATION.SOLUTIONS], selectedSolution.slug, true))

        },
        onLoaded: () => {
            dispatch(setNavigationLoading('solutions', true))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Solutions)
