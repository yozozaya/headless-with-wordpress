import { connect } from 'react-redux'
import {
    toggleShowAbout,
} from '../../actions'
import AboutModal from '../../components/About/AboutModal'

const mapStateToProps = (state) => {
    const { isLoading,
        isOpen,
        aboutContent
         } = state.about
    const { lang } = state.navigation
    return {
        isLoading,
        isOpen,
        aboutContent,
        lang
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClick: () => {
            dispatch(toggleShowAbout(true))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AboutModal)