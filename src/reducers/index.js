import { combineReducers } from 'redux'

import {
    // About Content
    REQUEST_ABOUT_CONTENT,
    RECEIVE_ABOUT_CONTENT,
    TOGGLE_SHOW_ABOUT_MODAL,
    // Industries List
    REQUEST_INDUSTRIES_LIST,
    RECEIVE_INDUSTRIES_LIST,
    TOGGLE_SHOW_INDUSTRIES_MODAL,
    SET_SELECTED_INDUSTRY,
    // Solutions List
    REQUEST_SOLUTIONS_LIST,
    RECEIVE_SOLUTIONS_LIST,
    TOGGLE_SHOW_SOLUTIONS_MODAL,
    SET_SELECTED_SOLUTION,
    // Design Content
    REQUEST_DESIGN_CONTENT,
    RECEIVE_DESIGN_CONTENT,
    //navigation
    SET_NAVIGATION_ITEM,
    TOGGLE_SHOW_MODAL_MENU,
    SET_ACTIVE_LANGUAGE,
    SET_INITIAL_SCROLL,
    SET_NAVIGATION_LOADING,
    // Contact
    REQUEST_CONTACT_CONTENT,
    RECEIVE_CONTACT_CONTENT,
    TOGGLE_SHOW_CONTACT_MODAL,

} from '../actions'


// Contact
const contactInitialState = {
    isLoading: false,
    isOpen: false,
}

const contact = (state = contactInitialState, action) => {
    switch (action.type) {
        case REQUEST_CONTACT_CONTENT:
            return {
                ...state,
                isLoading: true,
            }
        case RECEIVE_CONTACT_CONTENT:
            return {
                ...state,
                contactContent: action.contactContent,
                isLoading: false,
            }
        case TOGGLE_SHOW_CONTACT_MODAL:
            return {
                ...state,
                isOpen: !action.isOpen
            }
        default:
            return state
    }
}

// About List
const aboutInitialState = {
    isLoading: false,
    isOpen: false,
}

const about = (state = aboutInitialState, action) => {
    switch (action.type) {
        case REQUEST_ABOUT_CONTENT:
            return {
                ...state,
                isLoading: true,
            }
        case RECEIVE_ABOUT_CONTENT:
            return {
                ...state,
                aboutContent: action.aboutContent,
                isLoading: false,
            }
        case TOGGLE_SHOW_ABOUT_MODAL:
            return {
                ...state,
                isOpen: !action.isOpen
            }
        default:
            return state
    }
}

// Industries List
const industriesInitialState = {
    isLoading: false,
    selectedIndustry: null,
}

const industries = (state = industriesInitialState, action) => {
    switch (action.type) {
        case REQUEST_INDUSTRIES_LIST:
            return {
                ...state,
                isLoading: true,
            }
        case RECEIVE_INDUSTRIES_LIST:
            return {
                ...state,
                list: action.list,
                isLoading: false,
            }
        case TOGGLE_SHOW_INDUSTRIES_MODAL:
            return {
                ...state,
                isOpen: !action.isOpen
            }
        case SET_SELECTED_INDUSTRY:
            return {
                ...state,
                selectedIndustry: action.selectedIndustry,
            }
        default:
            return state
    }
}

// Solutions List
const solutionsInitialState = {
    isLoading: false,
    isOpen: false,
    selectedSolution: null,
    solutionsContent: null,
}

const solutions = (state = solutionsInitialState, action) => {
    switch (action.type) {
        case REQUEST_SOLUTIONS_LIST:
            return {
                ...state,
                isLoading: true,
            }
        case RECEIVE_SOLUTIONS_LIST:
            return {
                ...state,
                solutionsContent: action.solutionsContent,
                isLoading: false,
            }
        case TOGGLE_SHOW_SOLUTIONS_MODAL:
            return {
                ...state,
                isOpen: !action.isOpen
            }
        case SET_SELECTED_SOLUTION:
            return {
                ...state,
                selectedSolution: action.selectedSolution,
            }
        default:
            return state
    }
}


// Design List
const designInitialState = {
    isLoading: false,
}

const design = (state = designInitialState, action) => {
    switch (action.type) {
        case REQUEST_DESIGN_CONTENT:
            return {
                ...state,
                isLoading: true,
            }
        case RECEIVE_DESIGN_CONTENT:
            return {
                ...state,
                designContent: action.designContent,
                isLoading: false,
            }
        default:
            return state
    }
}


// Scroll To Navigation

const navigationInitialState = {
    navigationItem: null,
    isOpen: false,
    lang: 0,
    initialScroll: false,
    aboutLoaded: false,
    industriesLoaded: false,
    solutionsLoaded: false,
    designLoaded: false,
    contactLoaded: false,
}

const navigation = (state = navigationInitialState, action) => {
    const { type, payload } = action
    switch (type) {
        case SET_NAVIGATION_ITEM:
            return {
                ...state,
                navigationItem: action.navigationItem,
            }
        case TOGGLE_SHOW_MODAL_MENU:
            return {
                ...state,
                isOpen: !action.isOpen,
            }
        case SET_ACTIVE_LANGUAGE:
            return {
                ...state,
                lang: action.lang,
            }
        case SET_INITIAL_SCROLL:
            return {
                ...state,
                initialScroll: action.initialScroll,
            }
        case SET_NAVIGATION_LOADING:
            return {
                ...state,
                [`${payload.scope}Loaded`]: payload.loaded,
            }
        default:
            return state
    }
}

const peerlesspackApp = combineReducers({
    about,
    contact,
    industries,
    solutions,
    design,
    navigation
})

export default peerlesspackApp


